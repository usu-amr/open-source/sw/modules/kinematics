#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Float32MultiArray, MultiArrayDimension
from geometry_msgs.msg import Wrench, Accel
from numpy import linalg as LA
import numpy as np

is_enabled = False
center_gravity = None
inv_motor_wrench_matrix = None
set_powers_pub = None

# def enable_pipe(data):
#     global is_enabled
#     name = data.data
#     if name == 'kinematics':
#         rospy.loginfo('kinematics enabled')
#         is_enabled = True

# def disable_pipe(data):
#     global is_enabled
#     name = data.data
#     if name == 'kinematics':
#         rospy.loginfo('kinematics disabled')
#         is_enabled = False

# def apply_wrench(data):
#     global inv_motor_wrench_matrix, is_enabled
#     if is_enabled:
#         force = data.force
#         torque = data.torque
#         wrench = [
#             force.x,
#             force.y,
#             force.z,
#             torque.x,
#             torque.y,
#             torque.z
#         ]
#         # calc powers using (Moore-Penrose) pseudo-inverse
#         powers = np.dot(inv_motor_wrench_matrix, wrench)
#         set_powers_pub.publish(Float32MultiArray(data=powers))

def solve_for_motor_wrench(motor):
    global center_gravity
    force_direction = np.multiply(1 / LA.norm(motor['force_direction']), motor['force_direction'])
    max_force_magnitude = motor['max_force']
    motor_location = motor['location']
    # calc max force in vector form
    max_force = np.multiply(max_force_magnitude, force_direction)
    # calc torque in vector form
    r = np.subtract(motor_location, center_gravity)
    max_torque = np.cross(r, max_force)
    return [
        max_force[0],
        max_force[1],
        max_force[2],
        -max_torque[0],
        max_torque[1],
        max_torque[2]
    ]
    
def enable_accel_callback(data):
    global is_enabled
    is_enabled = data.data
    if not is_enabled:
        # set motors to off
        power_array = [0, 0, 0, 0, 0, 0, 0]
        msg = Float32MultiArray(data=power_array)
        msg.layout.dim.insert(0, MultiArrayDimension())
        msg.layout.dim[0].size = len(power_array)
        msg.layout.dim[0].stride = 1
        set_powers_pub.publish(msg)
    
def cmd_accel_callback(data):
    global inv_motor_wrench_matrix, is_enabled
    if is_enabled:
        linear = data.linear
        angular = data.angular
        wrench = [
            linear.x,
            linear.y,
            linear.z,
            angular.x,
            angular.y,
            angular.z
        ]
        # calc powers using (Moore-Penrose) pseudo-inverse
        powers = np.dot(inv_motor_wrench_matrix, wrench)
        power_array = np.asarray(powers)
        power_array = np.insert(power_array, 0, 1)
        msg = Float32MultiArray(data=power_array)
        msg.layout.dim.insert(0, MultiArrayDimension())
        msg.layout.dim[0].size = len(power_array)
        msg.layout.dim[0].stride = 1
        set_powers_pub.publish(msg)

def node():
    global center_gravity, inv_motor_wrench_matrix, set_powers_pub
    rospy.init_node('kinematics')
    
    # rospy.Subscriber("/control_mode/set", String, control_mode_set)
    # rospy.Subscriber("/control_mode/enable_pipe", String, enable_pipe)
    # rospy.Subscriber("/control_mode/disable_pipe", String, disable_pipe)
    rospy.Subscriber("/enable_accel", Bool, enable_accel_callback, queue_size=1)
    
    # rospy.Subscriber("/kinematics/apply_wrench", Wrench, apply_wrench)
    rospy.Subscriber("/cmd_accel", Accel, cmd_accel_callback, queue_size=1)
    
    # set_powers_pub = rospy.Publisher('/motors/set_powers', Float32MultiArray, queue_size=10)
    set_powers_pub = rospy.Publisher('/cmd_motors', Float32MultiArray, queue_size=1)
    
    # load params to do kinematic calculations
    center_gravity = rospy.get_param("/center_gravity")
    motor_wrenches = map(solve_for_motor_wrench, rospy.get_param("/motors"))
    motor_wrench_matrix = np.transpose(motor_wrenches)
    inv_motor_wrench_matrix = LA.pinv(motor_wrench_matrix, rcond=1e-8)
    
    # rospy.loginfo("\n" + np.array2string(motor_wrench_matrix, max_line_width=1000))
    
    rate = rospy.Rate(100) # 100hz
    while not rospy.is_shutdown():
        rate.sleep()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass